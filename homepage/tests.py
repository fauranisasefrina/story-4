from django.test import Client, TestCase
from django.urls import resolve
from homepage.views import efrin

# Create your tests here.
def test_url_is_exist(self):
    response = Client().get('')
    self.assertEqual(response.status_code, 200)

def test_using_efrin_template(self):
    response = Client().get('')
    self.assertTemplateUsed(response, 'efrin.html')

def test_using_efrin_func(self):
    found = resolve('')
    self.assertEqual(found.func, efrin)

    