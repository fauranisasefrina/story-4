from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *
from .models import *
from .forms import *
import time

# Create your tests here.
class Lab6UnitTest(TestCase):
	def test_url_exist(self): #test url
		response = Client().get('/kegiatan/')
		self.assertEqual(response.status_code,200)
	def test_url_using_func(self): #test fungsi
		found = resolve('/kegiatan/')
		self.assertEqual(found.func, createKegiatan)
	def test_create_models(self):
		new_model = listKegiatan.objects.create(namaKegiatan ='sabeb', id=3)
		new_model2 = listSiswa.objects.create(namaSiswa = 'siswa', kegiatan = new_model)
		counting_new_model = listKegiatan.objects.all().count()
		self.assertEqual(str(new_model), new_model.namaKegiatan)
		self.assertEqual(str(new_model2), new_model2.namaSiswa)
		self.assertEqual(counting_new_model,1)
		post = self.client.post('/create/3', data={'namaSiswa':'siswa'})
	def test_form_is_blank(self): 
		form = formKegiatan(data= {'namaKegiatan':''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
                form.errors['namaKegiatan'],
                ["This field is required."]
                )
	def test_form_template_used(self):
		response = Client().get('/kegiatan/')
		self.assertTemplateUsed(response, 'kegiatan.html')
	def test_post_form(self):
		response = Client().post('/kegiatan/',{
				'namaKegiatan':'hai'
			})
            
		self.assertIn('hai',response.content.decode())





