from django.urls import path,include
from django.shortcuts import render
from django.conf.urls import url
from . import views

app_name = 'story6'

urlpatterns = [
    path('kegiatan/', views.createKegiatan, name = 'kegiatan'),
    path('create/<int:siswa_id>', views.createSiswa, name = 'create')
    # dilanjutkan ...
]