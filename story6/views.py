from django.shortcuts import render, redirect
from .forms import *
from .models import *

# Create your views here.

def createKegiatan(request):
	kegiatan= listKegiatan.objects.all()
	kegiatann=formKegiatan(request.POST or None)
	context={
		'kegiatan':kegiatan,
		'kegiatann':kegiatann
	}
	if request.method=='POST':
		if kegiatann.is_valid():
			kegiatan.create(
				namaKegiatan=kegiatann.cleaned_data.get('namaKegiatan')
				)
	return render(request,'kegiatan.html',context)

def createSiswa(request, siswa_id):
	siswa= listKegiatan.objects.get(id=siswa_id)
	siswaTarget = listSiswa.objects.all()
	form_siswa = formSiswa(request.POST or None)
	context={
		'siswa':siswa,
		'form_siswa':form_siswa,
		'siswa_target':siswaTarget,
	}
	if request.method=='POST':
		if form_siswa.is_valid():
			siswaTarget.create( #memasukkan siswa ke kegiatan tertentu
					namaSiswa=form_siswa.cleaned_data.get('namaSiswa'),
					kegiatan=siswa
				)
	return render(request,'peserta.html',context)



