from django.test import TestCase, Client
from django.urls import resolve
from additionalpages.views import organisasi, kepanitiaan, matakuliah, savematakuliah, showmatakuliah, hapusmatakuliah, detail

# Create your tests here.

class AdditionalPagesUnitTest(TestCase) :

    def test_organisasi_url_is_exist(self):
        response = Client().get('/organisasi/')
        self.assertEqual(response.status_code, 200)

    def test_organisasi_using_organisasi_template(self):
        response = Client().get('/organisasi/')
        self.assertTemplateUsed(response, 'organisasi.html')
    
    def test_organisasi_using_organisasi_func(self):
        found = resolve('/organisasi/')
        self.assertEqual(found.func, organisasi)

    
    # def test_kepanitiaan_url_is_exist(self):
    #     response = Client().get('/kepanitiaan/')
    #     self.assertEqual(response.status_code, 200)
    
    # def test_kepanitiaan_using_kepanitiaan_template(self):
    #     response = Client().get('/kepanitiaan/')
    #     self.assertTemplateUsed(response, 'kepanitiaan.html')

    # def test_kepanitiaan_using_kepanitiaan_func(self):
    #     found = resolve('/kepanitiaan/')
    #     self.assertEqual(found.func, kepanitiaan)

    
    # def test_matakuliah_url_is_exist(self):
    #     response = Client().get('/matakuliah/')
    #     self.assertEqual(response.status_code, 200)
    
    # def test_matakuliah_using_matakuliah_template(self):
    #     response = Client().get('/matakuliah/')
    #     self.assertTemplateUsed(response, 'matakuliah.html')

    # def test_matakuliah_using_matakuliah_func(self):
    #     found = resolve('/matakuliah/')
    #     self.assertEqual(found.func, matakuliah)


    # def test_savematakuliah_url_is_exist(self):
    #     response = Client().get('/matakuliah/savematakuliah/')
    #     self.assertEqual(response.status_code, 200)

    # def test_savematakuliah_using_show_template(self):
    #     response = Client().get('/matakuliah/savematakuliah/')
    #     self.assertTemplateUsed(response, '/showmatakuliah/')

    # def test_savematakuliah_using__func(self):
    #     found = resolve('/matakuliah/savematakuliah/')
    #     self.assertEqual(found.func, savematakuliah)



    # def test_showmatakuliah_url_is_exist(self):
    #     response = Client().get('/showmatakuliah/')
    #     self.assertEqual(response.status_code, 200)

    # def test_showmatakuliah_using_matakuliah_template(self):
    #     response = Client().get('/showmatakuliah/')
    #     self.assertTemplateUsed(response, 'matakuliah.html')
    
    # def test_organisasi_using_organisasi_func(self):
    #     found = resolve('showmatakuliah/')
    #     self.assertEqual(found.func, showmatakuliah)



    # def test_showsavematakuliah_url_is_exist(self):
    #     response = Client().get('/showmatakuliah/savematakuliah/')
    #     self.assertEqual(response.status_code, 200)

    # def test_savematakuliah_using_kepanitiaan_template(self):
    #     response = Client().get('/showmatakuliah/savematakuliah/')
    #     self.assertTemplateUsed(response, 'kepanitiaan.html')

    # def test_organisasi_using_organisasi_func(self):
    #     found = resolve('/showmatakuliah/savematakuliah/')
    #     self.assertEqual(found.func, organisasi)

    

    # def test_hapusmatakuliah_url_is_exist(self):
    #     response = Client().get('/hapusmatakuliah/<int:id>/')
    #     self.assertEqual(response.status_code, 200)

    # def test_savematakuliah_using_kepanitiaan_template(self):
    #     response = Client().get('/hapusmatakuliah/<int:id>/')
    #     self.assertTemplateUsed(response, 'kepanitiaan.html')

    # def test_organisasi_using_organisasi_func(self):
    #     found = resolve('/hapusmatakuliah/<int:id>/')
    #     self.assertEqual(found.func, organisasi)

    

    # def test_detailmatakuliah_url_is_exist(self):
    #     response = Client().get('/detailmatakuliah/<int:id>/')
    #     self.assertEqual(response.status_code, 200)
    
    # def test_savematakuliah_using_kepanitiaan_template(self):
    #     response = Client().get('/detailmatakuliah/<int:id>/')
    #     self.assertTemplateUsed(response, 'kepanitiaan.html')

    # def test_organisasi_using_organisasi_func(self):
    #     found = resolve('/detailmatakuliah/<int:id>/')
    #     self.assertEqual(found.func, organisasi)

    

    
    