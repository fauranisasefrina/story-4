
from additionalpages.models import MataKuliah
from django import forms

class Input_Form(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = "__all__" #ambil semua yang terdapat pada model

#dari sini kebawah
    error_messages = {
        'required' : 'Please Type'
    }
    input_attrs = {
        'type' : 'text',
        'placeholder' : 'Nama Mata Kuliah'
    }

    input_attrs1 = {
        'type' : 'text',
        'placeholder' : 'Nama Dosen Pengajar'
    }

    input_attrs2 = {
        'type' : 'text',
        'placeholder' : 'jumlah SKS'
    }

    input_attrs3 = {
        'type' : 'text',
        'placeholder' : 'Deskripsi Mata Kuliah'
    }

    input_attrs4 = {
        'type' : 'text',
        'placeholder' : 'Semester Tahun'
    }

    input_attrs5 = {
        'type' : 'text',
        'placeholder' : 'Ruang Kelas'
    }

    nama_mata_kuliah = forms.CharField(label='', required=True,
        max_length=20, widget=forms.TextInput(attrs=input_attrs))
    dosen_pengajar = forms.CharField(label='', required=True,
        max_length=30, widget=forms.TextInput(attrs=input_attrs1))
    jumlah_sks = forms.IntegerField(label='', required=True, widget=forms.TextInput(attrs=input_attrs2)) 
    deskripsi_mata_kuliah = forms.CharField(label='', required=True,
        max_length=40, widget=forms.TextInput(attrs=input_attrs3))
    semester_tahun = forms.CharField(label='', required=True,
        max_length=20, widget=forms.TextInput(attrs=input_attrs4))
    ruang_kelas = forms.CharField(label='', required=True,
        max_length=10, widget=forms.TextInput(attrs=input_attrs5))



    



    


