# Create your models here.
from django.db import models

class MataKuliah(models.Model): #class model
    nama_mata_kuliah = models.CharField(max_length=20, blank= False)
    dosen_pengajar = models.CharField(max_length=30, blank= False)
    jumlah_sks = models.IntegerField(blank= False)
    deskripsi_mata_kuliah = models.CharField(max_length=40, blank= False)
    semester_tahun = models.CharField(max_length=20, blank= False)
    ruang_kelas = models.CharField(max_length=10, blank= False)

class Meta :
    db_table = 'dt_matakuliah' #menamai tabel pada database







