from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from additionalpages.models import MataKuliah
from additionalpages.forms import Input_Form
from django.template import response
from additionalpages import models
# Create your views here
    
def organisasi(request):
    response = {'title_html' : 'Organisasi'}
    return render(request, 'organisasi.html', response)

def kepanitiaan(request):
    response = {'title_html' : 'Kepanitiaan'}
    return render(request, 'kepanitiaan.html', response)


def matakuliah(request):
    response = {'input_form' : Input_Form(), 'title_html' : 'Mata Kuliah'}
    return render(request, 'matakuliah.html', response)

#ngambil form dari forms.py
def savematakuliah(request):
    form = Input_Form(request.POST or None)
    if (request.method == 'POST') :
        if (form.is_valid()) :
            form.save()
            return HttpResponseRedirect('/showmatakuliah')
        else :
            return HttpResponseRedirect('/')
    else :
        return HttpResponseRedirect('/')

#menampilan matakuliah yang sudah ditambahkan
def showmatakuliah(request):
    matakuliah = MataKuliah.objects.all() #mengambil data dari data base melalui models
    response= {'matkuls' : matakuliah} #ini untuk ngisi variabel
    return render(request, 'matakuliah.html', response)

def hapusmatakuliah(request, id):
    matakuliah = MataKuliah.objects.get(id=id)
    matakuliah.delete()
    return HttpResponseRedirect('/showmatakuliah')

def detail(request,id) :
    matakuliah = MataKuliah.objects.get(id=id)
    response={'matkuls' : matakuliah, 'title_html' : 'Detail Mata Kuliah' }
    return render(request, 'detailmatakuliah.html', response)



